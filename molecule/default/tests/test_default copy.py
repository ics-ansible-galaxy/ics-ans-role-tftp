import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ubuntu')


@pytest.fixture()
def tftpfile(host):
    """Fixture that creates an empty file in the tftp root dir"""
    name = 'mytest'
    filename = '/var/lib/tftpboot/' + name
    # host.run('touch {}'.format(filename))
    yield name
    # Remove the file created in the tftp root dir
    # as well as the file downloaded by the tftp command
    # in the current directory
    host.run('rm -f {} {}'.format(filename, name))


@pytest.fixture()
def tftpcmd2(host):
    """Fixture that installs the tftp command before a test"""
    host.run('apt install -y tftp-hpa')
    yield
    host.run('apt remove -y tftp-hpa')


def test_xinted_enabled_and_running(host):
    service = host.service('xinetd')
    assert service.is_enabled
    assert service.is_running


def test_tftp_server2(host, tftpcmd2, tftpfile):
    assert not host.file(tftpfile).exists
    cmd = host.run('tftp 127.0.0.1 -c get {}'.format(tftpfile))
    assert cmd.rc == 0
    # assert host.file(tftpfile).exists
