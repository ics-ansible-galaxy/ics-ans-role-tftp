import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('centos')


@pytest.fixture()
def tftpfile(host):
    """Fixture that creates an empty file in the tftp root dir"""
    name = 'mytest'
    filename = os.path.join('/var/lib/tftpboot', name)
    host.run('touch {}'.format(filename))
    yield name
    # Remove the file created in the tftp root dir
    # as well as the file downloaded by the tftp command
    # in the current directory
    host.run('rm -f {} {}'.format(filename, name))


@pytest.fixture()
def tftpcmd(host):
    """Fixture that installs the tftp command before a test"""
    host.run('yum install -y tftp')
    yield
    host.run('yum remove -y tftp')


def test_xinted_enabled_and_running(host):
    service = host.service('xinetd')
    assert service.is_enabled
    assert service.is_running


def test_tftp_server(host, tftpcmd, tftpfile):
    assert not host.file(tftpfile).exists
    cmd = host.run('tftp 127.0.0.1 -c get {}'.format(tftpfile))
    assert cmd.rc == 0
    assert host.file(tftpfile).exists
