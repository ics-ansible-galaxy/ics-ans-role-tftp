ics-ans-role-tftp
=================

Ansible role to install tftp.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
tftp_root_directory: /var/lib/tftpboot
tftp_server_args: "-c -s "
tftp_disable: "no"
tftp_socket_type: "dgram"
tftp_protocol: "udp"
tftp_wait: "yes"
tftp_user: "root"
tftp_group: "root"
tftp_mode: "0755"
tftp_server: "/usr/sbin/in.tftpd"
tftp_per_source: "11"
tftp_cps: "100 2"
tftp_flags: "IPv4"
tftp_packages:
  - tftp-server
  - xinetd
tftp_service: xinetd
tftp_config: /etc/xinetd.d/tftp
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-tftp
```

License
-------

BSD 2-clause
